#!/usr/bin/bash
rosservice call /reset
rosservice call /turtle1/set_pen "{r: 255, g: 128, b: 128, width: 2, 'off': 1}"
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-2.0 , 2.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rosservice call /turtle1/set_pen "{r: 255, g: 128, b: 128, width: 2, 'off': 0}"
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0 , -5.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0 , 1.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0, 10.0, 0.0]' '[0.0, 0.0, -9.42]'
rosservice call /turtle1/set_pen "{r: 128, g: 255, b: 128, width: 2, 'off': 0}"
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-3.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[3.0 , 3.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-3.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'

