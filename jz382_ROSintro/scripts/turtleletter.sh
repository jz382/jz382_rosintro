#!/usr/bin/bash
rosservice call /reset
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-2.0 , 2.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0 , -5.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0 , 1.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0, 10.0, 0.0]' '[0.0, 0.0, -9.42]'
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-3.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[3.0 , 3.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -l /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-3.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'

